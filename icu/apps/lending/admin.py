# _*_ coding: utf-8 _*_
from django.contrib import admin
from icu.apps.lending.models import Item, Lending


class AdminItem(admin.ModelAdmin):
    list_display = ('name', 'description', 'lending_duration', 'locked')
    list_filter = ('name', 'locked')


class AdminLending(admin.ModelAdmin):
    list_display = ('item', 'user', 'start_date', 'end_date')

admin.site.register(Item, AdminItem)
admin.site.register(Lending, AdminLending)