# -*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from icu.apps.lending.models import  Item, Lending
import datetime
import time


class LendingTest(TestCase):
    fixtures = ["test_data.json"]

    def test_list(self):
        """
        Tests item list page
        """
        client = Client()
        response = client.get(reverse("item_list"))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "camera")
        self.assertContains(response, "playstation")
        self.assertContains(response, "phone")

    def test_login_required(self):
        """
        Tests if Calendar and Borrow views are protected
        """
        client = Client()
        response = client.get(reverse("calendar"))
        self.assertEquals(response.status_code, 302)
        response = client.get(reverse("borrow"))
        self.assertEquals(response.status_code, 302)

    def test_calendar(self):
        """
        Tests calendar view
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("calendar"))
        self.assertEquals(response.status_code, 200)

    def test_borrow(self):
        """
        Tests borrow view
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("borrow"))
        self.assertEquals(response.status_code, 200)

    def test_calendar_filters(self):
        """
        Tets calendar's filters - no filters
        """
        user1 = User.objects.get(username="lending1")
        user2 = User.objects.get(username="lending2")
        item1 = Item.objects.get(name="camera")
        item2 = Item.objects.get(name="playstation")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        td2 = datetime.timedelta(days=3)
        Lending.objects.create(item=item1, user=user1, start_date=today, end_date=today+td1)
        Lending.objects.create(item=item2, user=user2, start_date=today+td2, end_date=today+td1+td2)
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("calendar"))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "camera")
        self.assertContains(response, "lending1")
        self.assertContains(response, "playstation")
        self.assertContains(response, "lending2")

    def test_calendar_filters_item(self):
        """
        Tets calendar's filters for item
        """
        user1 = User.objects.get(username="lending1")
        user2 = User.objects.get(username="lending2")
        item1 = Item.objects.get(name="camera")
        item2 = Item.objects.get(name="playstation")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        td2 = datetime.timedelta(days=3)
        td3 = datetime.timedelta(days=15)
        start_month = today - td3
        end_month = today + td3
        start_time = int(time.mktime(start_month.timetuple()))
        end_time = int(time.mktime(end_month.timetuple()))
        Lending.objects.create(item=item1, user=user1, start_date=today, end_date=today+td1)
        Lending.objects.create(item=item2, user=user2, start_date=today+td2, end_date=today+td1+td2)
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("calendar_lendings")+"?item=%s&start=%d&end=%d&_=%d" % (str(item1.id), start_time, end_time, int(time.time())))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "camera")
        self.assertContains(response, "lending1")
        self.assertNotContains(response, "playstation")
        self.assertNotContains(response, "lending2")

    def test_calendar_filters_user(self):
        """
        Tets calendar's filters for user
        """
        user1 = User.objects.get(username="lending1")
        user2 = User.objects.get(username="lending2")
        item1 = Item.objects.get(name="camera")
        item2 = Item.objects.get(name="playstation")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        td2 = datetime.timedelta(days=3)
        td3 = datetime.timedelta(days=15)
        start_month = today - td3
        end_month = today + td3
        start_time = int(time.mktime(start_month.timetuple()))
        end_time = int(time.mktime(end_month.timetuple()))
        Lending.objects.create(item=item1, user=user1, start_date=today, end_date=today+td1)
        Lending.objects.create(item=item2, user=user2, start_date=today+td2, end_date=today+td1+td2)
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("calendar_lendings")+"?user=%s&start=%d&end=%d&_=%d" % (str(user2.id), start_time, end_time, int(time.time())))
        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, "camera")
        self.assertNotContains(response, "lending1")
        self.assertContains(response, "playstation")
        self.assertContains(response, "lending2")

    def test_borrow_correct_items(self):
        """
        Tests if in borrow page the drop down has coorect items
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.get(reverse("borrow"))
        self.assertContains(response, "camera")
        self.assertContains(response, "playstation")
        self.assertNotContains(response, "phone")

    def test_borrow_post_no_data(self):
        """
        Tests borrow post with no data
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.post(reverse("borrow"), data={})
        self.assertContains(response, "Error")

    def test_borrow_post_only_item(self):
        """
        Tests borrow post with no dates
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.post(reverse("borrow"), data={"item": "1"})
        self.assertContains(response, "Error")

    def test_borrow_post_no_end(self):
        """
        Tests borrow post with no end date
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.post(reverse("borrow"), data={"item": "1", "start_date": datetime.date.today().strftime("%d/%m/%Y")})
        self.assertContains(response, "Error")

    def test_borrow_post_no_start(self):
        """
        Tests borrow post with no start date
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        response = client.post(reverse("borrow"), data={"item": "1", "end_date": datetime.date.today().strftime("%d/%m/%Y")})
        self.assertContains(response, "Error")

    def test_borrow_start_less_today(self):
        """
        Test start_date less that today
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        previous = today - td1
        response = client.post(reverse("borrow"), data={"start_date": previous.strftime("%d/%m/%Y"), "end_date": today.strftime("%d/%m/%Y"), "item": "1"})
        self.assertContains(response, "The start date entered is less than today's date.")

    def test_borrow_end_less_start(self):
        """
        Test end_date < start_date
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        next = today + td1
        response = client.post(reverse("borrow"), data={"start_date": next.strftime("%d/%m/%Y"), "end_date": today.strftime("%d/%m/%Y"), "item": "1"})
        self.assertContains(response, "The end date entered is less than the start date.")

    def test_borrow_duration(self):
        """
        Test duration
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=10)
        end = today + td1
        response = client.post(reverse("borrow"), data={"start_date": today.strftime("%d/%m/%Y"), "end_date": end.strftime("%d/%m/%Y"), "item": "2"})
        self.assertContains(response, "The duration entered is greater than the maximun duration allowed for this item.")

    def test_borrow_overlappings(self):
        """
        Test overlappings
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        today = datetime.date.today()
        user1 = User.objects.get(username="lending1")
        user2 = User.objects.get(username="lending2")
        item1 = Item.objects.get(name="camera")
        item2 = Item.objects.get(name="playstation")
        td1 = datetime.timedelta(days=5)
        td2 = datetime.timedelta(days=3)
        Lending.objects.create(item=item1, user=user1, start_date=today, end_date=today+td1)
        Lending.objects.create(item=item2, user=user2, start_date=today+td2, end_date=today+td1+td2)
        end = today + td2
        response = client.post(reverse("borrow"), data={"start_date": today.strftime("%d/%m/%Y"), "end_date": end.strftime("%d/%m/%Y"), "item": "1"})
        self.assertContains(response, "Item not available at the dates you entered.")
        end = today + td1
        response = client.post(reverse("borrow"), data={"start_date": today.strftime("%d/%m/%Y"), "end_date": end.strftime("%d/%m/%Y"), "item": "2"})
        self.assertContains(response, "Item not available at the dates you entered.")
        start = today + td2
        end = today + td1 + td2
        response = client.post(reverse("borrow"), data={"start_date": start.strftime("%d/%m/%Y"), "end_date": end.strftime("%d/%m/%Y"), "item": "1"})
        self.assertContains(response, "Item not available at the dates you entered.")

    def test_borrow_success(self):
        """
        Test borrow success
        """
        client = Client()
        client.login(username="lending1", password="lending1")
        today = datetime.date.today()
        td1 = datetime.timedelta(days=5)
        end = today + td1
        response = client.post(reverse("borrow"), data={"start_date": today.strftime("%d/%m/%Y"), "end_date": end.strftime("%d/%m/%Y"), "item": "1"})
        self.assertContains(response, "Success")