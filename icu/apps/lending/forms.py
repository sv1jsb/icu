# -*- coding: utf-8 -*-
from datetime import date, timedelta
from django import forms
from django.db.models import Q
from icu.apps.lending.models import Lending, Item


class LendingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LendingForm, self).__init__(*args, **kwargs)
        self.fields['item'].queryset = Item.objects.exclude(locked=True)

    def clean(self):
        cleaned_data = super(LendingForm, self).clean()
        start_date = cleaned_data.get('start_date', None)
        end_date = cleaned_data.get('end_date', None)
        item = cleaned_data.get('item', None)
        if not (start_date and end_date and item):
            return cleaned_data
        if start_date < date.today():
            raise forms.ValidationError("The start date entered is less than today's date.")
        if end_date < start_date:
            raise forms.ValidationError("The end date entered is less than the start date.")
        if item:
            if item.lending_duration and end_date - start_date > timedelta(days=item.lending_duration):
                raise forms.ValidationError("The duration entered is greater than the maximun duration allowed for this item.")
            lendings = Lending.objects.filter(Q(item=item),
                                              Q(start_date__lte=start_date, end_date__gte=end_date) |
                                              Q(start_date__range=(start_date, end_date)) |
                                              Q(end_date__range=(start_date, end_date))
                                              ).distinct()
            if len(lendings) > 0:
                raise forms.ValidationError("Item not available at the dates you entered.")
        return cleaned_data

    class Meta:
        model = Lending
        exclude = ('user', )
        widgets = {
            "item": forms.Select(attrs={"class": "form-control", "onchange": "$('#calendar').fullCalendar('refetchEvents')"}),
            "start_date": forms.DateInput(attrs={"class": "form-control"}),
            "end_date": forms.DateInput(attrs={"class": "form-control"}),
            "comment": forms.Textarea(attrs={"class": "form-control"}),
        }
