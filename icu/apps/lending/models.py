# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Item(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    image = models.ImageField(upload_to='item_images', blank=True, default="item_images/no_image.png")
    lending_duration = models.IntegerField(blank=True, null=True)
    locked = models.BooleanField(default=False, blank=True)

    def __unicode__(self):
        return self.name


class Lending(models.Model):
    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
    start_date = models.DateField()
    end_date = models.DateField()
    comment = models.TextField(blank=True)

    def __unicode__(self):
        return self.item.name
