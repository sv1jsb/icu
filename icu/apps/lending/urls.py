from django.conf.urls import patterns, url
from icu.apps.lending.views import ItemList, calendar, calendar_lendings, borrow


urlpatterns = patterns('',
    url(r'^$', ItemList.as_view(), {}, "item_list"),
    url(r'^calendar/$', calendar, {}, "calendar"),
    url(r'^calendar/lendings/$', calendar_lendings, {}, "calendar_lendings"),
    url(r'^borrow/$', borrow, {}, "borrow"),
)
