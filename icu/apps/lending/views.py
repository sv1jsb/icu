from django.views.generic import ListView
from django.http import HttpResponse
from django.db.models import Q
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from icu.apps.lending.models import Item, Lending
from icu.apps.lending.forms import LendingForm
import json
from datetime import datetime


class ItemList(ListView):
    model = Item
    template_name = "item_list.html"
    context_object_name = "items"


@login_required
def calendar(request):
    items = Item.objects.all()
    users = User.objects.all()
    context_vars = {"items": items, "users": users}
    template_name = "calendar.html"
    context = RequestContext(request)
    return render_to_response(template_name, context_vars,  context_instance=context)


@login_required
def calendar_lendings(request):
    #colors = ["#006400", "#556B2F", "#808000", "#6B8E23", "#008000", "#2E8B57"]
    ret = []
    from_date = datetime.fromtimestamp(float(request.GET['start']))
    to_date = datetime.fromtimestamp(float(request.GET['end']))
    lendings = Lending.objects.filter(Q(start_date__range=(from_date, to_date)) | Q(end_date__range=(from_date, to_date))).distinct()
    if request.GET.get('item', None):
        lendings = lendings.filter(item_id=request.GET['item'])
    if request.GET.get('user', None):
        lendings = lendings.filter(user_id=request.GET['user'])
    for lending in lendings:
        if lending.user == request.user:
            color = "#22B24C"
        else:
            color = "#6B8E23"
        ret.append({"title": lending.item.name+" - "+lending.user.username,
                    "start": lending.start_date.strftime("%Y-%m-%d"),
                    "end": lending.end_date.strftime("%Y-%m-%d"),
                    "color": color,
                    "allDay": True,
                    "comment": lending.comment
        })
    return HttpResponse(content=json.dumps(ret), content_type="application/json")


@login_required
def borrow(request):
    context_vars = {}
    if request.method == "POST":
        form = LendingForm(request.POST)
        if form.is_valid():
            lending = form.save(commit=False)
            lending.user = request.user
            lending.save()
            form = LendingForm()
            context_vars.update({"success": "Your submission was successful"})
        else:
            ve = ""
            if '__all__' in form.errors:
                ve += u"{0}<br/>".format(form.errors['__all__'][0])
            for field in form:
                if field.errors:
                    for error in field.errors:
                        ve += u"{0}: {1}<br/>".format(field.label, error)
            context_vars.update({"failure": ve})
    else:
        initial = {}
        if request.GET.get('item', None):
            initial.update({"item": request.GET["item"]})
        form = LendingForm(initial=initial)
    context_vars.update({"form": form})
    template_name = "borrow.html"
    context = RequestContext(request)
    return render_to_response(template_name, context_vars,  context_instance=context)