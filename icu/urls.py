from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$',TemplateView.as_view(template_name="documentation.html")),
    url(r'^lending/', include('icu.apps.lending.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve',  #
            {'document_root': settings.STATIC_ROOT}),               # Remove these two urls
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',   # in production
            {'document_root': settings.MEDIA_ROOT}),                #
)